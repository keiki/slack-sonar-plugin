package org.bitbucket.keiki.sonar.slack.slack;

import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackMessage;

public class SlackClient {

    public void send(String webhook, SlackMessage slackMessage) {
        SlackApi api = new SlackApi(webhook);
        api.call(slackMessage);
    }
}
