package org.bitbucket.keiki.sonar.slack.sonar.web.api;

import java.util.List;

public class Facets {
    private String property;
    private List<IssueCounter> values;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public List<IssueCounter> getValues() {
        return values;
    }

    public void setValues(List<IssueCounter> values) {
        this.values = values;
    }
}
