package org.bitbucket.keiki.sonar.slack.slack;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import java.util.List;
import java.util.Optional;
import org.bitbucket.keiki.sonar.slack.sonar.SlackPlugin;

public class WebHook {

    private final String patternType;
    private final String pattern;
    private final String webhook;
    private final boolean sendOk;


    public WebHook(String patternType, String pattern, String webhook, boolean sendOk) {
        this.patternType = patternType;
        this.pattern = pattern;
        this.webhook = webhook;
        this.sendOk = sendOk;
    }

    public String getPatterType() {
        return patternType;
    }

    public String getPattern() {
        return pattern;
    }

    public String getWebhook() {
        return webhook;
    }

    public boolean isSendOk() {
        return sendOk;
    }

    public boolean matches(String projectName) {
        if (SlackPlugin.PATTERN_TYPE_EQUALS.equals(patternType)) {
            return projectName.equals(pattern);
        }

        return projectName.contains(pattern);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(patternType, pattern, webhook, sendOk);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof WebHook) {
            WebHook that = (WebHook) object;
            return Objects.equal(this.patternType, that.patternType) && Objects.equal(this.pattern, that.pattern)
                    && Objects.equal(this.webhook, that.webhook) && Objects.equal(this.sendOk, that.sendOk);
        }

        return false;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("patternType", patternType).add("pattern", pattern)
                .add("webhook", webhook).add("sendOk", sendOk).toString();
    }

    public static Optional<WebHook> findHookIn(SlackPluginSettings settings, String name) {
        List<WebHook> hooks = settings.getHooks();
        for (WebHook hook : hooks) {

            if (hook.matches(name)) {
                return Optional.of(hook);
            }
        }
        return Optional.empty();
    }
}
