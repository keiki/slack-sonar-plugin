package org.bitbucket.keiki.sonar.slack.sonar;

import org.sonar.api.Plugin;
import org.sonar.api.Properties;
import org.sonar.api.Property;
import org.sonar.api.PropertyField;
import org.sonar.api.PropertyType;

/**
 * This class is the entry point for all extensions
 */
@Properties({
  @Property(
    key = SlackPlugin.WEBHOOK_KEY,
    name = "Slack incoming webhook",
    description = "A message will be sent to the first webhook where the project name matches the pattern",
    fields = {
      @PropertyField(
        key = SlackPlugin.PATTERN_TYPE_KEY,
        name = "Project pattern type",
        type = PropertyType.SINGLE_SELECT_LIST,
        options = {
          SlackPlugin.PATTERN_TYPE_CONTAINS,
          SlackPlugin.PATTERN_TYPE_EQUALS}),
      @PropertyField(
        key = SlackPlugin.PATTERN_KEY,
        name = "Project pattern",
        type = PropertyType.STRING),
      @PropertyField(
        key = SlackPlugin.WEBHOOK_URL_KEY,
        name = "Slack webhook",
        type = PropertyType.STRING,
        indicativeSize = 60),
      @PropertyField(
        key = SlackPlugin.SEND_OK_KEY,
        name = "Send OK message",
        description = "default: " + SlackPlugin.SEND_OK_DEFAULT,
        type = PropertyType.BOOLEAN
      ),
    }),
  @Property(
          key = SlackPlugin.OK_KEY,
          type = PropertyType.STRING,
          name = "Slack ok message",
          description = "The message sent if no new issues have been found"),
  @Property(
          key = SlackPlugin.SLACK_ICON_KEY,
          type = PropertyType.STRING,
          name = "Slack icon URL",
          description = "The URL for the slack chat icon")})
public final class SlackPlugin implements Plugin {

    public static final String WEBHOOK_KEY = "sonar.slack.webHook";
    public static final String PATTERN_TYPE_KEY = "patternTypeKey";
    public static final String PATTERN_TYPE_CONTAINS = "contains";
    public static final String PATTERN_TYPE_EQUALS = "equals";
    public static final String PATTERN_KEY = "patternKey";
    public static final String SEND_OK_KEY = "sendOkKey";
    public static final String SEND_OK = "true";
    public static final String SEND_OK_DEFAULT = SEND_OK;
    public static final String WEBHOOK_URL_KEY = "webhookUrlKey";
    public static final String OK_KEY = "sonar.slack.okMessage";
    public static final String SLACK_ICON_KEY = "sonar.slack.icon.url";

    @Override
    public void define(Context context) {
        context.addExtension(SendToSlack.class);
    }
}
