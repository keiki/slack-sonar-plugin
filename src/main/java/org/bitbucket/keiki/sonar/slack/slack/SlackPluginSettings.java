package org.bitbucket.keiki.sonar.slack.slack;

import static org.bitbucket.keiki.sonar.slack.sonar.SlackPlugin.PATTERN_KEY;
import static org.bitbucket.keiki.sonar.slack.sonar.SlackPlugin.PATTERN_TYPE_KEY;
import static org.bitbucket.keiki.sonar.slack.sonar.SlackPlugin.SEND_OK_KEY;
import static org.bitbucket.keiki.sonar.slack.sonar.SlackPlugin.WEBHOOK_URL_KEY;

import com.google.common.base.Splitter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import org.bitbucket.keiki.sonar.slack.sonar.SlackPlugin;
import org.sonar.api.config.Settings;

public class SlackPluginSettings {

    private static final String SONAR_SERVER_BASE_URL = "sonar.core.serverBaseURL";
    private final String serverUrl;
    private URI iconUrl;
    private String okayMessage;
    private List<WebHook> hooks = new ArrayList<>();


    public SlackPluginSettings(Settings settings) {
        iconUrl = URI.create(settings.getString(SlackPlugin.SLACK_ICON_KEY));
        okayMessage = settings.getString(SlackPlugin.OK_KEY);
        this.serverUrl = settings.getString(SONAR_SERVER_BASE_URL);

        readHooks(settings);
    }

    private void readHooks(Settings settings) {
        String ids = settings.getString(SlackPlugin.WEBHOOK_KEY);
        if (ids == null) {
            return;
        }
        Splitter.on(',').split(ids).forEach(s -> createSingleHook(settings, s));
    }

    private void createSingleHook(Settings settings, String id) {
        String prefix = SlackPlugin.WEBHOOK_KEY + "." + id + ".";
        String pattern = settings.getString(prefix + PATTERN_KEY);
        String patternType = settings.getString(prefix + PATTERN_TYPE_KEY);
        String webHook = settings.getString(prefix + WEBHOOK_URL_KEY);
        boolean sendOk = settings.getBoolean(prefix + SEND_OK_KEY);
        hooks.add(new WebHook(patternType, pattern, webHook, sendOk));
    }
    public String getServerUrl() {
        return serverUrl;
    }

    public URI getIconUrl() {
        return iconUrl;
    }

    public String getOkayMessage() {
        return okayMessage;
    }

    public List<WebHook> getHooks() {
        return hooks;
    }

    @Override
    public String toString() {
        return "SlackPluginSettings{" +
                "serverUrl='" + serverUrl + '\'' +
                ", iconUrl=" + iconUrl +
                ", okayMessage='" + okayMessage + '\'' +
                ", hooks=" + hooks +
                '}';
    }
}
