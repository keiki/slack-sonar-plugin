package org.bitbucket.keiki.sonar.slack.slack;

/**
 * Slack message colors ordered by significance.
 */
public enum MessageColor {
    DANGER("danger"),
    WARNING("warning"),
    GOOD("good");

    private String color;

    MessageColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }
}
