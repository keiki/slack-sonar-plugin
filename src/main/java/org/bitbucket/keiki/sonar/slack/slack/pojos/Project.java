package org.bitbucket.keiki.sonar.slack.slack.pojos;

import java.time.ZonedDateTime;
import java.util.Map;

public class Project {

    private String key;
    private String name;
    private ZonedDateTime analysedAt;
    private QualityGateStatus gateStatus;
    private int totalNewIssues;
    private Map<SeverityDomain, Integer> newIssuesCount;
    private Map<SeverityDomain, Integer> removedIssuesCount;
    private int successfulTests;
    private int failedTests;
    private float currentCodeCoverage;
    private float codeCoverageIncrease;

    public ZonedDateTime getAnalysedAt() {
        return analysedAt;
    }

    public void setAnalysedAt(ZonedDateTime analysedAt) {
        this.analysedAt = analysedAt;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public QualityGateStatus getGateStatus() {
        return gateStatus;
    }

    public void setGateStatus(QualityGateStatus gateStatus) {
        this.gateStatus = gateStatus;
    }

    public Map<SeverityDomain, Integer> getNewIssuesCount() {
        return newIssuesCount;
    }

    public void setNewIssuesCount(Map<SeverityDomain, Integer> newIssuesCount) {
        this.newIssuesCount = newIssuesCount;
    }

    public Map<SeverityDomain, Integer> getRemovedIssuesCount() {
        return removedIssuesCount;
    }

    public void setRemovedIssuesCount(Map<SeverityDomain, Integer> removedIssuesCount) {
        this.removedIssuesCount = removedIssuesCount;
    }

    public int getSuccessfulTests() {
        return successfulTests;
    }

    public void setSuccessfulTests(int successfulTests) {
        this.successfulTests = successfulTests;
    }

    public int getFailedTests() {
        return failedTests;
    }

    public void setFailedTests(int failedTests) {
        this.failedTests = failedTests;
    }

    public float getCurrentCodeCoverage() {
        return currentCodeCoverage;
    }

    public void setCurrentCodeCoverage(float currentCodeCoverage) {
        this.currentCodeCoverage = currentCodeCoverage;
    }

    public float getCodeCoverageIncrease() {
        return codeCoverageIncrease;
    }

    public void setCodeCoverageIncrease(float codeCoverageIncrease) {
        this.codeCoverageIncrease = codeCoverageIncrease;
    }

    public int getTotalNewIssues() {
        return totalNewIssues;
    }

    public void setTotalNewIssues(int totalNewIssues) {
        this.totalNewIssues = totalNewIssues;
    }

    @Override
    public String toString() {
        return "Project{" +
                "key='" + key + '\'' +
                ", name='" + name + '\'' +
                ", analysedAt=" + analysedAt +
                ", gateStatus=" + gateStatus +
                ", newIssuesCount=" + newIssuesCount +
                ", removedIssuesCount=" + removedIssuesCount +
                ", successfulTests=" + successfulTests +
                ", failedTests=" + failedTests +
                ", currentCodeCoverage=" + currentCodeCoverage +
                ", codeCoverageIncrease=" + codeCoverageIncrease +
                '}';
    }
}
