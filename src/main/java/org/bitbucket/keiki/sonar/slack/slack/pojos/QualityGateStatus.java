package org.bitbucket.keiki.sonar.slack.slack.pojos;

public class QualityGateStatus {
    private Status status;
    private int procecssingDurationinMs;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getProcecssingDurationinMs() {
        return procecssingDurationinMs;
    }

    public void setProcecssingDurationinMs(int procecssingDurationinMs) {
        this.procecssingDurationinMs = procecssingDurationinMs;
    }
}
