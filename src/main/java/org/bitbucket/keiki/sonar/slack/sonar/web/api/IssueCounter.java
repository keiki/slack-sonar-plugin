package org.bitbucket.keiki.sonar.slack.sonar.web.api;

public class IssueCounter {
    private String val;
    private int count;

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
