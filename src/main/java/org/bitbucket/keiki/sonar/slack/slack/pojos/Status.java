package org.bitbucket.keiki.sonar.slack.slack.pojos;

public enum Status {
    SUCCESS, FAILED, NOT_AVAILABLE
}
