package org.bitbucket.keiki.sonar.slack.slack.pojos;

public enum SeverityDomain {
    BLOCKER, CRITICAL, MAJOR, MINOR, INFO
}
