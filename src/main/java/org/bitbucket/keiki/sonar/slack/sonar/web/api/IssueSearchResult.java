package org.bitbucket.keiki.sonar.slack.sonar.web.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class IssueSearchResult {
    private int total;
    private List<Facets> facets;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Facets> getFacets() {
        return facets;
    }

    public void setFacets(List<Facets> facets) {
        this.facets = facets;
    }
}
