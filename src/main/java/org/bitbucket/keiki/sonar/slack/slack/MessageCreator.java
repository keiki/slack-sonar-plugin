package org.bitbucket.keiki.sonar.slack.slack;

import static com.google.common.net.UrlEscapers.urlFormParameterEscaper;
import static org.bitbucket.keiki.sonar.slack.slack.MessageColor.DANGER;
import static org.bitbucket.keiki.sonar.slack.slack.MessageColor.GOOD;
import static org.bitbucket.keiki.sonar.slack.slack.MessageColor.WARNING;
import static org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain.BLOCKER;
import static org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain.CRITICAL;
import static org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain.INFO;
import static org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain.MAJOR;
import static org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain.MINOR;

import java.util.Map;

import net.gpedro.integrations.slack.SlackAttachment;
import net.gpedro.integrations.slack.SlackMessage;

import org.bitbucket.keiki.sonar.slack.slack.pojos.Project;
import org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain;

public class MessageCreator {


    private static final String ISSUES_SEPARATOR = "  ";

    private final SlackPluginSettings settings;

    public MessageCreator(SlackPluginSettings settings) {
        this.settings = settings;
    }

    public SlackMessage getMessage(Project project) {
        SlackMessage message = new SlackMessage("");
        message.addAttachments(createAttachment(project));
        message.setIcon(settings.getIconUrl().toString());
        return message;
    }

    private SlackAttachment createAttachment(Project project) {
        SlackAttachment attachment = new SlackAttachment();
        attachment.setFallback("new issues " + project.getTotalNewIssues());
        attachment.setTitle(getTitle(project));
        MessageColor color = getColor(project.getNewIssuesCount());

        if (color == GOOD) {
            attachment.setText(settings.getOkayMessage());
        } else {
            attachment.setText(getMessageText(project.getNewIssuesCount()));
        }
        attachment.setColor(color.getColor());

        return attachment;
    }

    private String getTitle(Project project) {
        return "Project: <" + getUrl(project) + "|" + project.getName() + ">";
    }

    private String getUrl(Project project) {
        return settings.getServerUrl() + "/dashboard?id=" + urlFormParameterEscaper().escape(project.getKey());
    }

    private MessageColor getColor(Map<SeverityDomain, Integer> severityToCount) {
        if (severityToCount.get(BLOCKER) > 0
                || severityToCount.get(CRITICAL) > 0
                || severityToCount.get(MAJOR) > 0) {
            return DANGER;
        }
        if (severityToCount.get(MINOR) > 0
                || severityToCount.get(INFO) > 0) {
            return WARNING;
        }
        return GOOD;
    }

    private String getMessageText(Map<SeverityDomain, Integer> severityToCount) {
        StringBuilder message = new StringBuilder();
        message.append("New Blocker: ");
        message.append(severityToCount.get(BLOCKER));
        message.append(ISSUES_SEPARATOR);
        message.append("Critical: ");
        message.append(severityToCount.get(CRITICAL));
        message.append(ISSUES_SEPARATOR);
        message.append("Major: ");
        message.append(severityToCount.get(MAJOR));
        message.append(ISSUES_SEPARATOR);
        message.append("Minor: ");
        message.append(severityToCount.get(MINOR));
        message.append(ISSUES_SEPARATOR);
        message.append("Info: ");
        message.append(severityToCount.get(INFO));

        return message.toString();
    }
}
