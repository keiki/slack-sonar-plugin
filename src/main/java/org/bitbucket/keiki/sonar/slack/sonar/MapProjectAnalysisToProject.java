package org.bitbucket.keiki.sonar.slack.sonar;

import org.bitbucket.keiki.sonar.slack.slack.pojos.Project;
import org.bitbucket.keiki.sonar.slack.slack.pojos.QualityGateStatus;
import org.sonar.api.ce.posttask.PostProjectAnalysisTask.ProjectAnalysis;
import org.sonar.api.ce.posttask.QualityGate;
import org.sonar.api.ce.posttask.QualityGate.Status;

import java.util.Date;

import static org.bitbucket.keiki.sonar.slack.slack.pojos.Status.FAILED;
import static org.bitbucket.keiki.sonar.slack.slack.pojos.Status.NOT_AVAILABLE;
import static org.bitbucket.keiki.sonar.slack.slack.pojos.Status.SUCCESS;

public class MapProjectAnalysisToProject {

    private MapProjectAnalysisToProject() {
        //stateless static call
    }

    public static Project map(ProjectAnalysis projectAnalysis) {
        Project project = mapProject(projectAnalysis.getProject());
        project = mapQualityGate(project, projectAnalysis.getQualityGate());
        return project;
    }

    private static Project mapQualityGate(Project project, QualityGate qualityGate) {
        QualityGateStatus gateStatus = new QualityGateStatus();
        project.setGateStatus(gateStatus);
        if (qualityGate == null) {
            gateStatus.setStatus(NOT_AVAILABLE);
            return project;
        }
        Status status = qualityGate.getStatus();
        gateStatus.setStatus(mapStatus(status));
        return project;
    }

    private static org.bitbucket.keiki.sonar.slack.slack.pojos.Status mapStatus(Status status) {
        switch (status) {
            case OK:
                return SUCCESS;
            case ERROR:
            case WARN:
                return FAILED;
            default:
                    throw new IllegalArgumentException("Don't know the status " + status);
        }
    }

    private static Project mapProject(org.sonar.api.ce.posttask.Project project) {

        Project projectDomain = new Project();
        projectDomain.setKey(project.getKey());
        projectDomain.setName(project.getName());
        return projectDomain;
    }
}
