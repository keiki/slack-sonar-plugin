package org.bitbucket.keiki.sonar.slack.sonar;

import java.util.Optional;
import net.gpedro.integrations.slack.SlackMessage;
import org.bitbucket.keiki.sonar.slack.slack.MessageCreator;
import org.bitbucket.keiki.sonar.slack.slack.SlackClient;
import org.bitbucket.keiki.sonar.slack.slack.SlackPluginSettings;
import org.bitbucket.keiki.sonar.slack.slack.WebHook;
import org.bitbucket.keiki.sonar.slack.slack.pojos.Project;
import org.bitbucket.keiki.sonar.slack.sonar.web.api.IssueSearch;
import org.sonar.api.ce.posttask.PostProjectAnalysisTask;
import org.sonar.api.config.Settings;
import org.sonar.api.utils.log.Logger;
import org.sonar.api.utils.log.Loggers;

public class SendToSlack implements PostProjectAnalysisTask {

    private static final Logger LOG = Loggers.get(SendToSlack.class);
    /**
     * Sonar configuration. The underlying configuration changes per execution.
     */
    private final Settings sonarSettings;

    public SendToSlack(org.sonar.api.config.Settings settings) {
        this.sonarSettings = settings;
    }

    @Override
    public void finished(ProjectAnalysis projectAnalysis) {
        SlackPluginSettings settings = new SlackPluginSettings(sonarSettings);
        Project project = MapProjectAnalysisToProject.map(projectAnalysis);
        LOG.info("sonarSettings " + settings);
        LOG.info("project " + project);
        Optional<WebHook> hook = WebHook.findHookIn(settings, project.getName());

        if (!hook.isPresent()) {
            LOG.info("No matching hook found. Sending no message.");
            return;
        }
        fillIssueCount(projectAnalysis, settings, project);

        if (hook.get().isSendOk() || project.getTotalNewIssues() > 0) {

            MessageCreator messageCreator = new MessageCreator(settings);
            SlackMessage message = messageCreator.getMessage(project);

            SlackClient slackRequest = new SlackClient();
            slackRequest.send(hook.get().getWebhook(), message);
            LOG.info("Send slack message successfully for project '{}'", project.getName());
        }
    }

    private void fillIssueCount(ProjectAnalysis projectAnalysis, SlackPluginSettings settings, Project project) {
        projectAnalysis.getAnalysisDate().map(date -> new IssueSearch(settings).search(project, date))
                          .orElseThrow(() -> new IllegalArgumentException("No analyse timestamp was provided"));
    }
}
