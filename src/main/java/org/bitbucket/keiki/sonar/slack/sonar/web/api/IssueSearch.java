package org.bitbucket.keiki.sonar.slack.sonar.web.api;

import static org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain.BLOCKER;
import static org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain.CRITICAL;
import static org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain.INFO;
import static org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain.MAJOR;
import static org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain.MINOR;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import org.bitbucket.keiki.sonar.slack.slack.SlackPluginSettings;
import org.bitbucket.keiki.sonar.slack.slack.pojos.Project;
import org.bitbucket.keiki.sonar.slack.slack.pojos.SeverityDomain;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.sonar.api.rule.Severity;
import org.sonar.api.utils.log.Logger;
import org.sonar.api.utils.log.Loggers;

public class IssueSearch {

    private static final Logger LOG = Loggers.get(IssueSearch.class);
    private final SlackPluginSettings settings;

    public IssueSearch(SlackPluginSettings settings) {
        this.settings = settings;
    }

    public Project search(Project project, Date analisedTimestamp) {
        ZonedDateTime dateTime = ZonedDateTime.ofInstant(analisedTimestamp.toInstant(), ZoneOffset.UTC);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssxx", Locale.ENGLISH);
        String s = dateTimeFormatter.format(dateTime);


        Client client = ClientBuilder.newClient().register(JacksonFeature.class);
        WebTarget target = client.target(settings.getServerUrl()).path("api").path("issues").path("search");

        Form form = new Form();
        form.param("createdAt", s);
        form.param("facetMode", "count");
        form.param("facets", "severities");
        form.param("sinceLeakPeriod", "false");
        form.param("projectKeys", project.getKey());


        IssueSearchResult issues = target.request(MediaType.APPLICATION_JSON_TYPE)
                        .post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE),
                                IssueSearchResult.class);

        project.setNewIssuesCount(mapAddedIssuesToDomain(issues));
        project.setTotalNewIssues(issues.getTotal());
        LOG.info("issues " + project.getNewIssuesCount());
        return project;
    }

    private Map<SeverityDomain, Integer> mapAddedIssuesToDomain(IssueSearchResult issues) {
        List<IssueCounter> values = issues.getFacets().get(0).getValues();
        Map<SeverityDomain, Integer> severityToCount = new EnumMap<>(SeverityDomain.class);
        for (IssueCounter value : values) {
            severityToCount.put(mapSeverity(value.getVal()), value.getCount());
        }
        return severityToCount;

    }

    private SeverityDomain mapSeverity(String severity) {
        switch (severity) {
            case Severity.BLOCKER:
                return BLOCKER;
            case Severity.CRITICAL:
                return CRITICAL;
            case Severity.MAJOR:
                return MAJOR;
            case Severity.MINOR:
                return MINOR;
            case Severity.INFO:
                return INFO;
            default:
                throw new IllegalArgumentException("Unknown severity " + severity);
        }
    }

}
