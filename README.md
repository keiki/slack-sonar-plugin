# This plugin is not maintained anymore due to personal reasons

Last working version 7.9 LTS version


# Sonar Slack plugin

A [Sonar](www.sonarqube.org) plugin that will send messages to [Slack](https://slack.com/) after a Sonar run has been finished.

## Installation

Build project and copy to Sonar plugin folder

```bash
git clone https://bitbucket.org/keiki/slack-sonar-plugin.git
cd slack-sonar-plugin
mvn clean install

cp target/sonar-slack-plugin-<version>.jar <sonar-server>/opt/sonarqube/extensions/plugins/
```

## Configuration

![sonar-configuration.png](assets/sonar-configuration.png)

## Known issues

Sonar plugin will fail if the message could not be sent to slack.